
// (c) Copyright 2014 Caroline Schnapp. All Rights Reserved. Contact: mllegeorgesand@gmail.com
// See http://docs.shopify.com/manual/configuration/store-customization/advanced-navigation/linked-product-options
// Edited by Connor Munro 2015

var Shopify = Shopify || {};

Shopify.optionsMap = {};

Shopify.updateOptionsInSelector = function(selectorIndex, context, optionsMap) {

  switch (selectorIndex) {
    case 0:
      var key = 'root';
      var selector = jQuery("[data-product-id='" + context + "'] .single-option-selector:eq(0)");
      break;
    case 1:
      var key = jQuery("[data-product-id='" + context + "'] .single-option-selector:eq(0)").val();
      var selector = jQuery("[data-product-id='" + context + "'] .single-option-selector:eq(1)");
      break;
    case 2:
      var key = jQuery("[data-product-id='" + context + "'] .single-option-selector:eq(0)").val();  
      key += ' / ' + jQuery("[data-product-id='" + context + "'] .single-option-selector:eq(1)").val();
      var selector = jQuery("[data-product-id='" + context + "'] .single-option-selector:eq(2)");
  }
  
  var initialValue = selector.val();
  var availableOptions = optionsMap[key];
  var options = selector.find("option");
  var optionsLength = options.length;

  options.prop("disabled", false);

  if (availableOptions) {
    for (var i=0; i<availableOptions.length; i++) {
      var option = availableOptions[i];
      selector.find("option[value='" + option + "']").addClass("available");
    }
  }

  for (var i=0; i<optionsLength; i++) {
    var option = options.eq(i);
    if (!option.hasClass("available")) {
      option.prop("disabled", true);
      if (option.is(":selected")) {
        option
          .prop("selected", false)
          .next().prop("selected", true)
      }
    }
  }

  options.removeClass("available");

  jQuery('.swatch[data-option-index="' + selectorIndex + '"] .swatch-element').each(function() {
    if (jQuery.inArray($(this).attr('data-value'), availableOptions) !== -1) {
      $(this).removeClass('soldout').show().find(':radio').removeAttr('disabled','disabled').removeAttr('checked');
    }
    else {
      $(this).addClass('soldout').hide().find(':radio').removeAttr('checked').attr('disabled','disabled');
    }
  });
  if (jQuery.inArray(initialValue, availableOptions) !== -1) {
    selector.val(initialValue);
  }
  selector.trigger('change');
  
};

Shopify.linkOptionSelectors = function(product, context) {

  Shopify.optionsMap[context] = {};
  // Building our mapping object.
  for (var i=0; i<product.variants.length; i++) {
    tempVariant = null;
    var variant = product.variants[i];
    // Gathering values for the 1st drop-down.
    if (variant.available) {
      Shopify.optionsMap[context]['root'] = Shopify.optionsMap[context]['root'] || [];
      Shopify.optionsMap[context]['root'].push(variant.option1);
      Shopify.optionsMap[context]['root'] = Shopify.uniq(Shopify.optionsMap[context]['root']);
      // Gathering values for the 2nd drop-down.
      if (product.options.length > 1) {
        var key = variant.option1;
        Shopify.optionsMap[context][key] = Shopify.optionsMap[context][key] || [];
        Shopify.optionsMap[context][key].push(variant.option2);
        Shopify.optionsMap[context][key] = Shopify.uniq(Shopify.optionsMap[context][key]);
      }
      // Gathering values for the 3rd drop-down.
      if (product.options.length === 3) {
        var key = variant.option1 + ' / ' + variant.option2;
        Shopify.optionsMap[context][key] = Shopify.optionsMap[context][key] || [];
        Shopify.optionsMap[context][key].push(variant.option3);
        Shopify.optionsMap[context][key] = Shopify.uniq(Shopify.optionsMap[context][key]);
      }
    }
  }

  // Update options right away.
  Shopify.updateOptionsInSelector(0, context, Shopify.optionsMap[context]);
  if (product.options.length > 1) Shopify.updateOptionsInSelector(1, context, Shopify.optionsMap[context]);
  if (product.options.length === 3) Shopify.updateOptionsInSelector(2, context, Shopify.optionsMap[context]);
  // When there is an update in the first dropdown.
  jQuery("[data-product-id='" + context + "'] .single-option-selector:eq(0)").change(function() {
    if (product.options.length > 1) Shopify.updateOptionsInSelector(1, context, Shopify.optionsMap[context]);
    if (product.options.length === 3) Shopify.updateOptionsInSelector(2, context, Shopify.optionsMap[context]);
    return true;
  });
  // When there is an update in the second dropdown.
  jQuery("[data-product-id='" + context + "'] .single-option-selector:eq(1)").change(function() {
    if (product.options.length === 3) Shopify.updateOptionsInSelector(2, context, Shopify.optionsMap[context]);
    return true;
  });
  
};
